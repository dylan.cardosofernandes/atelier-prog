#include <iomanip>
#include <cmath>

#include "Facture.h"


void Facture::ajouterLigne(const LigneFacture& ligne){

}
void Facture::validerPanier(const Panier& panier){
    for(auto [a,q]: panier.getArticles()){
        LigneFacture l;
        l.article = true;
        l.nom = a.nom;
        l.prixU = a.getPrixUnitaire();
        l.reference = a.getReference();
        l.quantite = q;
        l.tauxTVA = a.getTauxTva();
        l.prixTotal = Produit::calculPrixTTC(a.getPrixUnitaire(), q, a.getTauxTva());;
    }
}

bool Facture::finaliser(){
    finalisee = true;
    setDate();
    setNumeroFacture();
    return finalisee;
}

void Facture::setDate(){
    std::chrono::time_point<std::chrono::system_clock> date;
    date = std::chrono::system_clock::now();
    dateFacture = std::chrono::system_clock::to_time_t(date);

}
void Facture::setNumeroFacture(){
    Facture::numFacture++;
    numFacture = Facture::numFacture;
}

std::ostream& operator<<(std::ostream& os, const Facture& facture){

}