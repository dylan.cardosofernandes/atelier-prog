#include <iostream>
#include <memory>
#include "Produit.h"
#include "Panier.h"
#include "Client.h"
// #include "StrategieFacturationBase.h"

int main() {
    // Nécessaire pour l'affichage des prix en € avec les virgules décimales
    // std::locale::global(std::locale("fr_FR.UTF-8"));

    /* Q5. Décommenter les tests ci-dessous et en ajouter au fur et à mesure */
    
    std::cout << "La boutique ouvre ses portes !" << std::endl;
    Produit packToutChoco;
    packToutChoco.setNom("Paquet cadeau \"Tout choco\"");
    packToutChoco.setReference("CHOCO_001");
    packToutChoco.setPrixUnitaire(2500); // en centimes!
    packToutChoco.setTauxTva(5); // en %

    Produit magazineCaramel;
    magazineCaramel.setNom("Le mensuel du caramel");
    magazineCaramel.setReference("MAGCARAMEL_001");
    magazineCaramel.setPrixUnitaire(1430);
    magazineCaramel.setTauxTva(20);

    Produit abo;
    abo.setNom("Service de courtage de pralines");
    abo.setReference("ABOCOURTAGE_001");
    abo.setPrixUnitaire(99000);
    abo.setTauxTva(20);

    std::cout << packToutChoco << std::endl;
    std::cout << magazineCaramel << std::endl;
    std::cout << abo << std::endl;

    Panier charriot;
    charriot.ajouterArticle(packToutChoco, 2);
    charriot.ajouterArticle(magazineCaramel, 12);
    charriot.ajouterArticle(abo, 1);
    std::cout << charriot << std::endl;

    charriot.retirerArticle(magazineCaramel, 6);
    charriot.ajouterArticle(abo, 1);
    std::cout << charriot << std::endl;
    /*
    Client personne;
    personne.setNom("Per Sonne\n3 avenue de Quelque Part\n00123 Ville");
    */

    /* Q9 Quel type de retour ici ? */
    // ... facturation = StrategieFacturationBase::creerFacturation(charriot, personne);
    // Facture f = /* appeler la méthode facturer(charriot, personne); */
    /* appeler la méthode ajouterSurcout(f, "Livraison", Prix{2000}, 20); pour
     * ajouter 20€ de frais de port */
    // std::cout << f << std::endl;
    // f.finaliser();
    // std::cout << f << std::endl;

    return 0;
}
