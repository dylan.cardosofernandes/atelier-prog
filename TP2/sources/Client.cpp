#include "Client.h"

// Q1.
const std::string& Client::getNom() const {
    return nom;
}
 
void Client::setNom(const std::string& n) {
    nom = n;
}
 
bool Client::isPremium() const {
    return premium;
}
 
void Client::setPremium(bool p) {
    premium = p;
}
 
const std::string& Client::getCodeTva() const {
    return codeTva;
}
 
void Client::setCodeTva(const std::string& c) {
    codeTva = c;
}