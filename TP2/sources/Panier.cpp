#include <algorithm>
#include <iomanip>
#include "Panier.h"
#include <iostream>

const std::map<Produit, int>& Panier::getArticles() const{
    return articles;
}

void Panier::ajouterArticle(const Produit& prod, int q){
    auto it = articles.find(prod);
    if(it != articles.end()){
        it -> second += q;
    }
    else{
        articles[prod] = q;
    }
    totalArticlesHT += Produit::calculPrixHT(prod.getPrixUnitaire(), q);
    totalArticlesTTC += Produit::calculPrixTTC(prod.getPrixUnitaire(), q, prod.getTauxTva());

    
}
void Panier::retirerArticle(const Produit& prod, int q){
    auto it = articles.find(prod);
    if(it != articles.end()){
        if(it -> second - q <= 0){
            articles.erase(prod);
        }
        else{
            it -> second -= q;
        }
    }
    totalArticlesHT -= Produit::calculPrixHT(prod.getPrixUnitaire(), q);
    totalArticlesTTC -= Produit::calculPrixTTC(prod.getPrixUnitaire(), q, prod.getTauxTva());
}



// Vous pourez vous inspirer du code ci-dessous pour les factures si vous
// souhaitez
std::ostream& operator<<(std::ostream& os, const Panier& panier) {

    os <<
    "┌─────────────────┬───────────────────────────────────┬──────────────────┬───────────────┬───────────────┬───────────────┐\n"
    "│    reference    │              nom                  │  prix unitaire   │   quantité    │    prix HT    │    prix TTC   |\n"
    "├─────────────────┼───────────────────────────────────┼──────────────────┼───────────────┼───────────────┼───────────────┤\n"
    ;

    for (const auto& [p,q]: panier.articles) {
        std::cout << p.getPrixUnitaire() << std::endl;
        std::cout << q << std::endl;

        Prix prixHT = Produit::calculPrixHT(p.getPrixUnitaire(), q);
        Prix prixTTC = Produit::calculPrixTTC(p.getPrixUnitaire(), q, p.getTauxTva());

        os << std::left
           << "│" << std::setw(17) << p.getReference()
           << "│" << std::setw(35) << p.getNom().substr(0, 36)
           << std::right
           << "│" << std::setw(19) << p.getPrixUnitaire() << " "
           << "│" << std::setw(14) << q << " "
           << "│" << std::setw(16) << prixHT << " "
           << "│" << std::setw(16) << (prixTTC) << " "
           << "│\n";

    }
    os <<
       "├─────────────────┴───────────────────────────────────┴──────────────────┴───────────────┴───────────────┴───────────────┤\n"
       "├────────────────────────────────────────────────────────────────────────────────────────┬───────────────┬───────────────┤\n" <<
       "│                                      total                                             │" <<
       std::setw(16) << panier.totalArticlesHT  << " │"   <<
       std::setw(16) << panier.totalArticlesTTC << " │\n" <<
       "└────────────────────────────────────────────────────────────────────────────────────────┴───────────────┴───────────────┘\n" ;
    return os;
}




