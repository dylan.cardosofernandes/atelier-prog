#ifndef PRODUIT_H
#define PRODUIT_H

#include "Prix.h"
#include <string>
 
class Produit {
private:

   // Un PRODUIT est facturé selon le régime pro si le code TVA est renseigné
   double tauxTva;
 
   // Un PRODUIT possède une référence
   std::string reference;
 
   // // Un PRODUIT a un prix unitaire
   Prix prixUnitaire;
 
public:
   // Quelques accesseurs...
    // Le nom du PRODUIT
   std::string nom;
 
   const std::string& getNom() const;
 
   void setNom(const std::string& nom);
 
   const std::string& getReference() const;
  
   void setReference(const std::string& ref);
 
   const double& getTauxTva() const;
 
   void setTauxTva(const double& t);
 
   const Prix& getPrixUnitaire() const;
 
   void setPrixUnitaire(const long& p);
   void setPrixUnitaire(const Prix& p);

   static const Prix calculPrixHT(const Prix& p, int quantite);
   static const Prix calculPrixTTC(const Prix& p, int quantite, int tva);
 
   friend bool operator==(const Produit& prod1, const Produit& prod2);
   friend bool operator!=(const Produit& prod1, const Produit& prod2);
   friend std::ostream& operator<<(std::ostream& os, const Produit& p);
   friend bool operator<(const Produit& prod1, const Produit& prod2);
 
 
 
};
bool operator==(const Produit& prod1, const Produit& prod2);
bool operator!=(const Produit& prod1, const Produit& prod2);
std::ostream& operator<<(std::ostream& os, const Produit& p);
bool operator<(const Produit& prod1, const Produit& prod2);
 
#endif //PRODUIT_H
