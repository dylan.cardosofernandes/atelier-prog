#ifndef CLIENT_H
#define CLIENT_H

#include <string>

class Client {
private:
    // Le nom du client
    std::string nom;

    // Un client est facturé selon le régime pro si le code TVA est renseigné
    std::string codeTva;

    // Le statut premium du client
    bool premium;

public:
    // Quelques accesseurs...

    const std::string& getNom() const;

    void setNom(const std::string& nom);

    const std::string& getCodeTva() const;

    void setCodeTva(const std::string& codeTva);

    bool isPremium() const;

    void setPremium(bool premium = true);

};


#endif //CLIENT_H
