#ifndef FACTURE_H
#define FACTURE_H

#include "Produit.h"
#include "Panier.h"
#include "Prix.h"
#include <string>
#include <chrono> // Pour l'horodatage, voir le TP précédent
#include <ctime>
#include <ostream>
#include <vector>
#include <iostream> 
using namespace std;
using namespace std::chrono;

// Q8. Créer la classe ou structure LigneFacture
struct LigneFacture{
    bool article;
    string reference;
    string nom;
    Prix prixU;
    int quantite;
    int tauxTVA;
    Prix prixTotal;
};




class Facture {
private:
	// Q8. Ajouter les attributs nécessaires
    std::vector<LigneFacture> LignesFacture;
    std::string adresse;
    int numFacture;
    Prix prixTotal;

    bool finalisee;

    time_t dateFacture;


public:
    // Q8. Ainsi que les méthodes manquantes
    void ajouterLigne(const LigneFacture& ligne);
    void validerPanier(const Panier& panier);
    bool finaliser();
    void setDate();
    void setNumeroFacture();

    friend std::ostream& operator<<(std::ostream& os, const Facture& facture);
};

std::ostream& operator<<(std::ostream& os, const Facture& facture);

#endif //FACTURE_H
