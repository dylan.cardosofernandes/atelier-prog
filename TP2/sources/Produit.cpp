#include "Produit.h"
#include "Prix.h"
// Q2.
 
#include "Client.h"
 
// Q1.
 
#include "Produit.h"
 
// Q2.
 
const std::string& Produit::getNom() const {
   return nom;
}
 
void Produit::setNom(const std::string& n) {
   nom = n;
}
 
const std::string& Produit::getReference() const{
   return reference;
}
  
void Produit::setReference(const std::string& ref){
      reference = ref;
}
 
const double& Produit::getTauxTva() const {
   return tauxTva;
}
 
void Produit::setTauxTva(const double& t) {
   tauxTva = t;
}
 
void Produit::setPrixUnitaire(const long& p){
   prixUnitaire.cents = p;
}
 
const Prix& Produit::getPrixUnitaire() const{
   return prixUnitaire;
}
 
void Produit::setPrixUnitaire(const Prix& p){
   prixUnitaire = p;
}

bool operator==(const Produit& prod1, const Produit& prod2){
   return prod1.reference == prod2.reference;
}
 
bool operator!=(const Produit& prod1, const Produit& prod2){
   return !(operator==(prod1, prod2));
}

bool operator<(const Produit& prod1, const Produit& prod2){
   return prod1.reference < prod2.reference;
}
std::ostream& operator<<(std::ostream& os, const Produit& p)
{
   os << ">>> Produit : " <<p.nom << "\nReference : " << p.reference << "\nPrix : " << p.prixUnitaire << "\nTVA : " << p.tauxTva << "%\n";

   return os;
}  

const Prix Produit::calculPrixHT(const Prix& p, int quantite){
    //Utilisation d'une des surchages d'opérateur défini dans Prix.h
    return p*quantite;
}
 
const Prix Produit::calculPrixTTC(const Prix& p, int quantite, int tva){
    long double tvaDouble = (1.0 + (double)tva/100);
    //Utilisation d'une des surchages d'opérateur défini dans Prix.h
    return p*quantite*tvaDouble;
}



