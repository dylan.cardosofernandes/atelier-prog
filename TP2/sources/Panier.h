#ifndef PANIER_H
#define PANIER_H

#include <string>
#include <map>
#include "Produit.h"
#include "Prix.h"


class Panier {
private:
    std::map<Produit, int> articles;
    Prix totalArticlesHT;
    Prix totalArticlesTTC;
 
public:
   // Quelques accesseurs...
    const std::map<Produit, int>& getArticles() const;

	void ajouterArticle(const Produit& prod, int q);
	void retirerArticle(const Produit& prod, int q);

   friend std::ostream& operator<<(std::ostream& os, const Panier& panier);
 
};
std::ostream& operator<<(std::ostream& os, const Panier& panier);
 
#endif //PANIER_H
