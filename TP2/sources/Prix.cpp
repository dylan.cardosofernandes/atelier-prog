#include <iomanip>
#include <cmath>
#include "Prix.h"

std::ostream& operator<<(std::ostream& os, const Prix& price) {
    os.imbue(std::locale(""));
    os << std::showbase << std::put_money(price.cents);
    return os;
}

Prix::Prix(long cents) : cents(cents) {}

Prix::operator long double() const {
    return cents;
}

Prix operator+(const Prix& p1, const Prix& p2) {
    return Prix(p1.cents + p2.cents);
}

Prix operator*(const Prix& p1, long q) {
    return Prix(p1.cents * q);
}

Prix operator*(long q, const Prix& p1) {
    return p1 * q;
}

Prix operator*(const Prix& p1, int q) {
    return Prix(p1.cents * (long)q);
}

Prix operator*(int q, const Prix& p1) {
    return p1 * q;
}

Prix operator*(const Prix& p1, long double q) {
    return Prix(std::lroundl(p1.cents * q));
}

Prix operator*(long double q, const Prix& p1) {
    return p1 * q;
}

Prix& Prix::operator+=(const Prix& p) {
    cents += p.cents;
    return *this;
}
Prix& Prix::operator-=(const Prix& p) {
    cents -= p.cents;
    return *this;
}



