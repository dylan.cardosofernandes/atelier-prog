//
// Created by lgeorget on 15/08/2021.
//

#ifndef PRICE_H
#define PRICE_H

#include <ostream>

struct Prix {
    long cents;

    explicit Prix(long cents = 0);

    explicit operator long double() const;

    Prix& operator+=(const Prix& p);

    friend std::ostream& operator<<(std::ostream& os, const Prix& price);
    friend Prix operator+(const Prix& p1, const Prix& p2);
    friend Prix operator*(const Prix& p1, long q);
    friend Prix operator*(long q, const Prix& p1);
    friend Prix operator*(const Prix& p1, int q);
    friend Prix operator*(int q, const Prix& p1);
    friend Prix operator*(const Prix& p1, long double q);
    friend Prix operator*(long double q, const Prix& p1);
};

std::ostream& operator<<(std::ostream& os, const Prix& price);
Prix operator+(const Prix& p1, const Prix& p2);
Prix operator*(const Prix& p1, long q);
Prix operator*(long q, const Prix& p1);
Prix operator*(const Prix& p1, int q);
Prix operator*(int q, const Prix& p1);
Prix operator*(const Prix& p1, long double q);
Prix operator*(long double q, const Prix& p1);

#endif //PRICE_H
