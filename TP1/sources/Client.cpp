#include "Client.h"

// Q1.

const std::string& Client::getNom() const {
// ...
}

void Client::setNom(const std::string& n) {
// ...
}

bool Client::isPremium() const {
// ...
}

void Client::setPremium(bool p) {
// ...
}

const std::string& Client::getCodeTva() const {
// ...
}

void Client::setCodeTva(const std::string& c) {
// ...
}

