#ifndef FACTURE_H
#define FACTURE_H

#include <string>
#include <chrono> // Pour l'horodatage, voir le TP précédent
#include <ostream>

// Q8. Créer la classe ou structure LigneFacture

class Facture {
private:
	// Q8. Ajouter les attributs nécessaires

public:
    // Q8. Ainsi que les méthodes manquantes
    void ajouterLigne(const LigneFacture& ligne);

    bool finaliser();

    friend std::ostream& operator<<(std::ostream& os, const Facture& facture);
};

std::ostream& operator<<(std::ostream& os, const Facture& facture);

#endif //FACTURE_H
