//
// Created by lgeorget on 28/08/2021.
//

#ifndef STRATEGIEFACTURATIONBASE_H
#define STRATEGIEFACTURATIONBASE_H


#include "Panier.h"
#include "Client.h"

#include <memory>

class StrategieFacturationBase {
protected:
    const Panier* panier = nullptr;
    const Client* client = nullptr;

public:
    // Il est essentiel d'avoir un destructeur virtuel lorsque l'on a des
    // classes-filles, pour être sûr que le destructeur de la classe-fille (le
    // type réel) appropriée soit appelée à la destruction de l'objet
    virtual ~StrategieFacturationBase() = default;
    static /* Q9 quel type ? */ creerFacturation(const Panier& panier, const Client& client);
    virtual void ajouterSurcout(Facture& facture, const std::string& denomination, const Prix& prix, long tauxTva);

    virtual Facture facturer(const Panier& panier, const Client& client);
};


#endif //STRATEGIEFACTURATIONBASE_H
