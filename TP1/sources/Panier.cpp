#include <algorithm>
#include <iomanip>
#include "Panier.h"
#include "Prix.h"
#include "Produit.h"


// Vous pourez vous inspirer du code ci-dessous pour les factures si vous
// souhaitez
std::ostream& operator<<(std::ostream& os, const Panier& panier) {
    os <<
    "┌─────────────────┬───────────────────────────────────┬──────────────────┬───────────────┬───────────────┬───────────────┐\n"
    "│    reference    │              nom                  │  prix unitaire   │   quantité    │    prix HT    │    prix TTC   |\n"
    "├─────────────────┼───────────────────────────────────┼──────────────────┼───────────────┼───────────────┼───────────────┤\n"
    ;
    for (const auto& [p,q]: panier.articles) {
        Prix prixHT = Produit::calculPrixHT(p.getPrixUnitaire(), q);
        Prix tva = Produit::calculTVA(p.getPrixUnitaire(), p.getTauxTva(), q);
        os << std::left
           << "│" << std::setw(17) << p.getReference()
           << "│" << std::setw(35) << p.getNom().substr(0, 36)
           << std::right
           << "│" << std::setw(19) << p.getPrixUnitaire() << " "
           << "│" << std::setw(14) << q << " "
           << "│" << std::setw(16) << prixHT << " "
           << "│" << std::setw(16) << (prixHT + tva) << " "
           << "│\n";

    }
    os <<
       "├─────────────────┴───────────────────────────────────┴──────────────────┴───────────────┴───────────────┴───────────────┤\n"
       "├────────────────────────────────────────────────────────────────────────────────────────┬───────────────┬───────────────┤\n" <<
       "│                                      total                                             │" <<
       std::setw(16) << panier.totalArticlesHT  << " │"   <<
       std::setw(16) << panier.totalArticlesTTC << " │\n" <<
       "└────────────────────────────────────────────────────────────────────────────────────────┴───────────────┴───────────────┘\n" ;
    return os;
}


